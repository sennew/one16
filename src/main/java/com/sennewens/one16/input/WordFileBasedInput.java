package com.sennewens.one16.input;

import com.sennewens.one16.generator.CombinationMode;
import com.sennewens.one16.service.WordService;
import com.sennewens.one16.data.WordCombinationDTO;
import com.sennewens.one16.input.converters.LineSeparatorInputConverter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service("fileBasedInput")
public class WordFileBasedInput {

    private WordService wordService;

    public WordFileBasedInput(WordService databaseWordService) {
        this.wordService = databaseWordService;
    }

    public String createWords(String fileName) {
        String content = readContentOfFile(fileName);

        Set<String> convertedInput = new HashSet<>();

        if (content.contains("\n")){
            convertedInput = LineSeparatorInputConverter.convertInput(content);
        }

        if (CollectionUtils.isEmpty(convertedInput)){
            throw new IllegalArgumentException("Something went wrong with converting the input.");
        }

        Set<WordCombinationDTO> combinations = wordService.generateWords(convertedInput, CombinationMode.TWO_PIECES);

        return combinations.stream()
                .map(combination -> combination.getCombination() + " = " + combination.getResultWord())
                .collect(Collectors.joining("\n"));
    }


    private String readContentOfFile(String fileName){
        String content = "";

        try {
            content = new String(new ClassPathResource("input/" + fileName).getInputStream().readAllBytes());
        } catch (IOException e) {
            throw new RuntimeException("Could not read file: " + fileName, e);
        }

        return content;
    }
}
