package com.sennewens.one16.data;

public interface WordCombination {

    String getCombination();
    String getResultWord();

}
