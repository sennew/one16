package com.sennewens.one16.generator;

import com.sennewens.one16.data.WordCombinationDTO;

import java.util.Set;

public interface WordGenerator {
    Set<WordCombinationDTO> generateWordCombinations(Set<String> characterGroup, int maxLengthResultWord);
}
