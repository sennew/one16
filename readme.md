# One16 - Spring Boot word generator - SW

The word generator shows all word combinations that together form a word of X characters from the given input.

The combination (characterGroups) that form the result word must also be present in the input.

Words can be generated out of 2 pieces or N pieces of characterGroups:

    REST input generates words out of N pieces of characterGroups
    .TXT input generates words out of 2 pieces of characterGroups

## Configuration (application.properties)


| Property    | Description |
| ------------- |:-------------:|
| result-word-size | Amount of characters in result word. |

### Default values
    result-word-size=6




## Installation

Use maven to build the project and build/start the docker image

```bash
mvn package docker:start
```

## Usage

###### REST

Generates all possible word combinations out of N-characterGroups and returns the output to the user.

```bash
curl --request POST "http://localhost:8080/api/file" --data-binary "@input.txt" --header "Content-Type: text/plain" >> output.txt
```

###### .TXT

Generates all possible word combinations out of 2-characterGroups and saves the output to an inmemory database ([H2-CONSOLE](http:localhost:8080/h2-console)).

    JDBC URL: jdbc:h2:mem:testdb

    USERNAME: username

    PASSWORD: password

## Time sheet

| Desciption    | Time spend |
| ------------- |:-------------:|
| architecture application | 3 hours |
| N-characterGroup combination algorithm | 1 hour 30 minutes |
| making application production ready  | 1 hour     |
