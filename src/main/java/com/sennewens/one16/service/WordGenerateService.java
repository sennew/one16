package com.sennewens.one16.service;

import com.sennewens.one16.data.WordCombinationDTO;
import com.sennewens.one16.generator.CombinationMode;
import com.sennewens.one16.output.OutputWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class WordGenerateService implements WordService {

    @Autowired
    private Environment env;

    private OutputWriter outputWriter;

    @Override
    public Set<WordCombinationDTO> generateWords(Set<String> listOfCharacterGroups, CombinationMode combinationMode) {
        Set<WordCombinationDTO> combinations = combinationMode.getWordGenerator().generateWordCombinations(listOfCharacterGroups, Integer.parseInt(env.getProperty("result-word-size")));

        if (outputWriter != null){
            outputWriter.writeWords(combinations);
        }

        return combinations;
    }

    public void setOutputWriter(OutputWriter outputWriter) {
        this.outputWriter = outputWriter;
    }
}
