package com.sennewens.one16.output;

import com.sennewens.one16.data.WordCombinationDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service("consoleOutputWriter")
public class ConsoleOutputWriter implements OutputWriter {

    @Override
    public void writeWords(Set<WordCombinationDTO> combinations) {
        combinations.forEach(combination -> System.out.println(combination.toString()));
    }
}
