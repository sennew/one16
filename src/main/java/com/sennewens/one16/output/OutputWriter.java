package com.sennewens.one16.output;

import com.sennewens.one16.data.WordCombinationDTO;

import java.util.List;
import java.util.Set;

public interface OutputWriter {

    void writeWords(Set<WordCombinationDTO> combinations);
}
