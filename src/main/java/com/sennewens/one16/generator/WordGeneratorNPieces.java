package com.sennewens.one16.generator;

import com.sennewens.one16.data.WordCombinationDTO;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;

import java.util.*;
import java.util.stream.Collectors;
import org.springframework.util.CollectionUtils;

public class WordGeneratorNPieces implements WordGenerator {

    private final Logger logger = LoggerFactory.getLogger(WordGeneratorNPieces.class);

    @Override
    public Set<WordCombinationDTO> generateWordCombinations(Set<String> characterGroup, int maxLengthResultWord) {

        List<String> wordsWithLengthOfResultWord = characterGroup
                .stream()
                .filter(word -> word.length() == maxLengthResultWord)
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(wordsWithLengthOfResultWord)){
            logger.info("No resultwords found with length:  " + maxLengthResultWord);
            return new HashSet<>();
        }

        characterGroup.removeAll(wordsWithLengthOfResultWord);

        List<List<Pair<String, List<Integer>>>> resultList = new ArrayList<>();

        for (String resultWord : wordsWithLengthOfResultWord) {
            for (String inputWord : characterGroup) {
                List<Pair<String, List<Integer>>> result = getCharacterGroupsWithPositionInResultWord(resultWord, resultWord, inputWord, characterGroup);
                if (!result.isEmpty()){
                    resultList.add(result);
                }
            }
        }

        return createWordCombinations(resultList);
    }

    protected List<Pair<String, List<Integer>>> getCharacterGroupsWithPositionInResultWord(String resultWord, String wordPart, String characterGroup, Set<String> wordList) {
        List<Pair<String, List<Integer>>> partsToFormWord = new ArrayList<>();

        if (wordPart.contains(characterGroup)) {

            List<Integer> indexesWhereCharacterGroupExists = determineIndexesWhereCharacterGroupExistsInResultWord(resultWord, characterGroup);
            partsToFormWord.add(Pair.of(characterGroup, indexesWhereCharacterGroupExists));

            List<String> remainingWordParts = Arrays.stream(wordPart.split(characterGroup)).filter(e -> !e.isBlank()).collect(Collectors.toList());

            Iterator<String> remainingWordPartIterator = remainingWordParts.iterator();

            while (remainingWordPartIterator.hasNext()) {
                String remainingWordPart = remainingWordPartIterator.next();

                for (String inputCharacterGroup : wordList){
                    List<Pair<String, List<Integer>>> foundParts = getCharacterGroupsWithPositionInResultWord(resultWord, remainingWordPart, inputCharacterGroup, wordList);

                    if (!foundParts.isEmpty()) {
                        remainingWordPartIterator.remove();
                        partsToFormWord.addAll(foundParts);
                        break;
                    }
                }
            }

            if (!remainingWordParts.isEmpty()){
                return new ArrayList<>();
            }
        }

        return partsToFormWord;
    }

    private List<Integer> determineIndexesWhereCharacterGroupExistsInResultWord(String resultWord, String validWordPart) {
        List<Integer> indexesInResultWordOfValidWordPart = new ArrayList<>();

        IntStream.range(0, resultWord.length())
                .forEach(value -> {
                    if ((value + validWordPart.length() <= resultWord.length())) {
                        String subString = resultWord.substring(value, value + validWordPart.length());
                        if (subString.equals(validWordPart)) {
                            indexesInResultWordOfValidWordPart.add(value);
                        }
                    }
                });

        return indexesInResultWordOfValidWordPart;
    }

    protected Set<WordCombinationDTO> createWordCombinations(List<List<Pair<String, List<Integer>>>> resultWordPossibleCharacterGroupCombinations){

        Set<WordCombinationDTO> combinations = new HashSet<>();

        for (List<Pair<String, List<Integer>>> characterGroupsToFormWord : resultWordPossibleCharacterGroupCombinations) {

            int maxIndex = findMaxPartIndex(characterGroupsToFormWord);

            String resultCombination = "";
            String resultWord = "";

            for (int startIndex = 0; startIndex <= maxIndex;){

                String characterGroupForIndex = findPartForIndex(startIndex, characterGroupsToFormWord);

                resultCombination += characterGroupForIndex;
                resultWord += characterGroupForIndex;

                startIndex += characterGroupForIndex.length();

                if (startIndex <= maxIndex){
                    resultCombination += " + ";
                }
            }

            WordCombinationDTO combination = new WordCombinationDTO(resultCombination, resultWord);
            combinations.add(combination);
        }

        return combinations;
    }

    private int findMaxPartIndex(List<Pair<String, List<Integer>>> resultPieces){
        return resultPieces.stream()
                .map(e -> e.getSecond())
                .flatMap(List::stream)
                .reduce(Integer.MIN_VALUE, Integer::max);
    }

    private String findPartForIndex(int index, List<Pair<String, List<Integer>>> resultPieces) {
        return resultPieces.stream()
                .filter(e -> e.getSecond().contains(index))
                .findFirst()
                .get()
                .getFirst();
    }
}
