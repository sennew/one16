package com.sennewens.one16.input;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(properties = {"result-word-size=7"})
public class WordRestInputTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void before(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void simpleTest() throws Exception {
        this.mockMvc.perform(
                post("/api/file")
                        .contentType(MediaType.TEXT_PLAIN)
                        .content("woorden\n" +
                                "wo\n" +
                                "rd\n" +
                                "o\n" +
                                "oo\n" +
                                "or\n" +
                                "en\n" +
                                "woor\n" +
                                "den"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("wo + o + rd + en = woorden")));
    }

    @Test
    public void testInvalidInput() throws Exception {
        this.mockMvc.perform(
                post("/api/file")
                        .contentType(MediaType.TEXT_PLAIN)
                        .content(""))
                .andExpect(status().is(400));
    }

}
