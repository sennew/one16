package com.sennewens.one16.generator;

import com.sennewens.one16.data.WordCombinationDTO;
import java.util.List;
import java.util.stream.Collectors;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WordGeneratorTwoPiecesTest {

    private WordGeneratorTwoPieces wordGeneratorTwoPieces = new WordGeneratorTwoPieces();

    @Test
    public void generateWordsOutOfTwoPiecesAndPresentInInputList(){
        int lengthOfResultWord = 5;
        Set<String> listOfCharacterGroups = new HashSet<>(Arrays.asList("a", "ppel", "appel", "ap", "pel", "app", "el", "appe", "l"));

        Set<WordCombinationDTO> combinations = wordGeneratorTwoPieces.generateWordCombinations(listOfCharacterGroups, lengthOfResultWord);
        assertEquals(4, combinations.size());

        List<String> characterGroupCombinations = combinations.stream()
                .map(WordCombinationDTO::getCombination)
                .collect(Collectors.toList());

        assertTrue(characterGroupCombinations.contains("ap+pel"));
        assertTrue(characterGroupCombinations.contains("app+el"));
        assertTrue(characterGroupCombinations.contains("a+ppel"));
        assertTrue(characterGroupCombinations.contains("appe+l"));
    }
}
