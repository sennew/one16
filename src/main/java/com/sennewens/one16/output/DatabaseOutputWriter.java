package com.sennewens.one16.output;

import com.sennewens.one16.data.WordCombinationDTO;
import com.sennewens.one16.data.WordCombinationEntity;
import com.sennewens.one16.repository.WordCombinationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("databaseOutputWriter")
public class DatabaseOutputWriter implements OutputWriter {

    @Autowired
    private WordCombinationRepository repository;

    @Override
    public void writeWords(Set<WordCombinationDTO> combinations) {
        List<WordCombinationEntity> entitiesToSave = combinations.stream()
                .map(WordCombinationEntity::new)
                .collect(Collectors.toList());

        repository.saveAll(entitiesToSave);
    }
}
