package com.sennewens.one16;

import com.sennewens.one16.input.WordFileBasedInput;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class One16Application {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(One16Application.class, args);
		WordFileBasedInput inputApi = applicationContext.getBeansOfType(WordFileBasedInput.class).get("fileBasedInput");
		inputApi.createWords("input.txt");
	}

}
