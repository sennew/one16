package com.sennewens.one16.generator;

import com.sennewens.one16.data.WordCombinationDTO;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.util.CollectionUtils;

public class WordGeneratorTwoPieces implements WordGenerator {

    @Override
    public Set<WordCombinationDTO> generateWordCombinations(Set<String> characterGroup, int maxLengthResultWord) {
        Set<WordCombinationDTO> listOfResultWords = new HashSet<>();

        WordDataStructure dataStructure = new WordDataStructure(characterGroup, maxLengthResultWord);

        int indexOne = 1;
        int indexTwo = maxLengthResultWord - 1;


        while (indexOne < maxLengthResultWord && indexTwo >= 1){

            List<String> characterGroupWithIndexOneLength = dataStructure.getAllCharacterGroupsWithLength(indexOne);
            List<String> characterGroupWithIndexTwoLength = dataStructure.getAllCharacterGroupsWithLength(indexTwo);

            if (CollectionUtils.isEmpty(characterGroupWithIndexOneLength) || CollectionUtils.isEmpty(characterGroupWithIndexTwoLength)){
                indexOne++;
                indexTwo--;
                continue;
            }

            characterGroupWithIndexOneLength.forEach(characterGroupIndexOne -> {
                listOfResultWords.addAll(characterGroupWithIndexTwoLength.stream()
                        .map(characterGroupIndexTwo -> new WordCombinationDTO(characterGroupIndexOne + "+" + characterGroupIndexTwo, characterGroupIndexOne + characterGroupIndexTwo))
                        .filter(combination -> dataStructure.isWordPresentInValidResultList(combination.getResultWord()))
                        .collect(Collectors.toSet()));
            });

            indexOne++;
            indexTwo--;

        }

        return listOfResultWords;
    }

    private class WordDataStructure {
        private Map<Integer, List<String>> mapOfLengthCharacterGroupAndCorrespondingGroups;
        private int lengthOfResultWord;

        public WordDataStructure(Set<String> listOfWordsAndCharacters, int lengthOfResultWord){
            this.lengthOfResultWord = lengthOfResultWord;
            this.mapOfLengthCharacterGroupAndCorrespondingGroups = listOfWordsAndCharacters.stream()
                    .filter(e -> e.length() <= lengthOfResultWord)
                    .collect(Collectors.groupingBy(String::length));

        }

        public List<String> getAllCharacterGroupsWithLength(int characterGroupLength){
            return mapOfLengthCharacterGroupAndCorrespondingGroups.get(characterGroupLength);
        }

        public boolean isWordPresentInValidResultList(String word){
            List<String> validWords = mapOfLengthCharacterGroupAndCorrespondingGroups.get(lengthOfResultWord);
            return validWords.contains(word);
        }
    }
}
