package com.sennewens.one16.input.converters;

import com.sennewens.one16.input.converters.LineSeparatorInputConverter;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InputConverterTest {

    @Test
    public void convertStringWithLineSeperatorTest(){
        final String input = "a\nppel\nappel\na";
        LineSeparatorInputConverter converter = new LineSeparatorInputConverter();

        Set<String> convertedInput = converter.convertInput(input);
        assertEquals(3, convertedInput.size());
        assertTrue(convertedInput.contains("a"));
        assertTrue(convertedInput.contains("ppel"));
        assertTrue(convertedInput.contains("appel"));
    }
}
