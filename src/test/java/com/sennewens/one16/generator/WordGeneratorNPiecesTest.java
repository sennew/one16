package com.sennewens.one16.generator;

import com.sennewens.one16.data.WordCombinationDTO;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.data.util.Pair;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class WordGeneratorNPiecesTest {

    private WordGeneratorNPieces wordGeneratorNPieces = new WordGeneratorNPieces();

    Set<String> wordList = new HashSet<>(Arrays.asList("woorden", "wo", "rd", "o", "oo", "or", "en", "woor", "den", "affiche", "af", "fiche"));

    @Test
    public void testGenerateWords() {
        Set<WordCombinationDTO> combinations = wordGeneratorNPieces.generateWordCombinations(wordList, 7);
        assertFalse(combinations.isEmpty());
    }

    @Test
    public void testGetCharacterGroupsWithPositionInResultWord() {
        List<Pair<String, List<Integer>>> characterGroupsWithPositionInResultWord = wordGeneratorNPieces.getCharacterGroupsWithPositionInResultWord("woorden", "woorden", "wo", wordList);
        assertFalse(characterGroupsWithPositionInResultWord.isEmpty());
        assertTrue(characterGroupsWithPositionInResultWord.contains(Pair.of("wo", Arrays.asList(0))));
        assertTrue(characterGroupsWithPositionInResultWord.contains(Pair.of("or", Arrays.asList(2))));
        assertTrue(characterGroupsWithPositionInResultWord.contains(Pair.of("den", Arrays.asList(4))));
    }

    @Test
    public void testGetCharacterGroupsWithPositionInResultWord_OverlappingCharIndex() {
        List<Pair<String, List<Integer>>> partsToFormWord = wordGeneratorNPieces.getCharacterGroupsWithPositionInResultWord("woorden", "woorden", "en", wordList);
        assertFalse(partsToFormWord.isEmpty());
        assertTrue(partsToFormWord.contains(Pair.of("en", Arrays.asList(5))));
        assertTrue(partsToFormWord.contains(Pair.of("rd", Arrays.asList(3))));
        assertTrue(partsToFormWord.contains(Pair.of("wo", Arrays.asList(0))));
        assertTrue(partsToFormWord.contains(Pair.of("o", Arrays.asList(1,2))));
    }

    @Test
    public void testCreateCharacterGroupCombinations(){
        List<Pair<String, List<Integer>>> resultWordCharacterGroupCombination = new ArrayList<>();
        resultWordCharacterGroupCombination.add(Pair.of("en", Arrays.asList(5)));
        resultWordCharacterGroupCombination.add(Pair.of("rd", Arrays.asList(3)));
        resultWordCharacterGroupCombination.add(Pair.of("woo", Arrays.asList(0)));

        List<WordCombinationDTO> combinations = new ArrayList(wordGeneratorNPieces.createWordCombinations(Arrays.asList(resultWordCharacterGroupCombination)));
        assertFalse(combinations.isEmpty());
        assertEquals("woo + rd + en", combinations.get(0).getCombination());
    }

    @Test
    public void testCreateCharacterGroupCombinations_OverlappingCharIndex() {
        List<Pair<String, List<Integer>>> resultWordCharacterGroupCombination = new ArrayList<>();
        resultWordCharacterGroupCombination.add(Pair.of("en", Arrays.asList(5)));
        resultWordCharacterGroupCombination.add(Pair.of("rd", Arrays.asList(3)));
        resultWordCharacterGroupCombination.add(Pair.of("wo", Arrays.asList(0)));
        resultWordCharacterGroupCombination.add(Pair.of("o", Arrays.asList(1, 2)));

        List<WordCombinationDTO> combinations = new ArrayList(wordGeneratorNPieces.createWordCombinations(Arrays.asList(resultWordCharacterGroupCombination)));
        assertFalse(combinations.isEmpty());
        assertEquals("wo + o + rd + en", combinations.get(0).getCombination());
    }

}
