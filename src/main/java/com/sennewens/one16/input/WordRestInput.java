package com.sennewens.one16.input;

import com.sennewens.one16.generator.CombinationMode;
import com.sennewens.one16.service.WordService;
import com.sennewens.one16.data.WordCombinationDTO;
import com.sennewens.one16.input.converters.LineSeparatorInputConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api")
public class WordRestInput {

    private final Logger logger = LoggerFactory.getLogger(WordRestInput.class);

    private WordService wordService;

    public WordRestInput(WordService simpleWordService){
        this.wordService = simpleWordService;
    }

    @RequestMapping(value = "file", method = RequestMethod.POST)
    public String createWords(@RequestBody String input) {
        logger.info("INCOMING POST REQUEST /api/info");

        Set<String> convertedList = LineSeparatorInputConverter.convertInput(input);

        Set<WordCombinationDTO> generatedWords = wordService.generateWords(convertedList, CombinationMode.N_PIECES);

        return generatedWords.stream()
                .sorted(Comparator.comparing(WordCombinationDTO::getResultWord))
                .map(WordCombinationDTO::toString)
                .collect(Collectors.joining("\n"));
    }
}
