package com.sennewens.one16.repository;

import com.sennewens.one16.data.WordCombinationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordCombinationRepository extends JpaRepository<WordCombinationEntity, Long> {
}
