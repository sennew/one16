package com.sennewens.one16.input;

import com.sennewens.one16.service.WordService;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = {"result-word-size=7"})
public class WordFileBasedInputTest {

    @Autowired
    private WordService simpleWordService;

    private WordFileBasedInput fileBasedInput;

    @BeforeEach
    public void before(){
        fileBasedInput = new WordFileBasedInput(simpleWordService);
    }

    @Test
    public void testFileBasedInput() {
        String output = fileBasedInput.createWords("testInput.txt");
        assertFalse(output.isEmpty());
        List<String> result = Arrays.asList(output.split("\n"));
        assertEquals(2, result.size());
        assertTrue(result.contains("woor+den = woorden"));
        assertTrue(result.contains("af+fiche = affiche"));
    }

    @Test
    public void testFileRead() {
        Assertions.assertThrows(RuntimeException.class, () -> fileBasedInput.createWords(null), "Could not read file: ");
    }

    @Test
    public void testEmptyFile() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> fileBasedInput.createWords("emptyInput.txt"), "Something went wrong with converting the input.");
    }
}
