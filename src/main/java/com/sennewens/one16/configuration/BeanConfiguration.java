package com.sennewens.one16.configuration;

import com.sennewens.one16.output.OutputWriter;
import com.sennewens.one16.service.WordGenerateService;
import com.sennewens.one16.service.WordService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    WordService simpleWordService(){
        return new WordGenerateService();
    }

    @Bean
    WordService databaseWordService(OutputWriter databaseOutputWriter){
        WordService service = new WordGenerateService();
        service.setOutputWriter(databaseOutputWriter);
        return service;
    }

    @Bean
    WordService consoleWordService(OutputWriter consoleOutputWriter){
        WordService service = new WordGenerateService();
        service.setOutputWriter(consoleOutputWriter);
        return service;
    }
}
