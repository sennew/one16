package com.sennewens.one16.generator;

import com.sennewens.one16.generator.WordGenerator;
import com.sennewens.one16.generator.WordGeneratorNPieces;
import com.sennewens.one16.generator.WordGeneratorTwoPieces;

public enum CombinationMode {
    TWO_PIECES(new WordGeneratorTwoPieces()),
    N_PIECES(new WordGeneratorNPieces());

    private WordGenerator wordGenerator;

    CombinationMode(WordGenerator wordGenerator){
        this.wordGenerator = wordGenerator;
    }

    public WordGenerator getWordGenerator(){
        return wordGenerator;
    }

}
