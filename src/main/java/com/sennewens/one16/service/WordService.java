package com.sennewens.one16.service;

import com.sennewens.one16.data.WordCombinationDTO;

import com.sennewens.one16.generator.CombinationMode;
import com.sennewens.one16.output.OutputWriter;
import java.util.Set;

public interface WordService {
    Set<WordCombinationDTO> generateWords(Set<String> listOfCharacterGroups, CombinationMode combinationMode);
    void setOutputWriter(OutputWriter outputWriter);
}
