package com.sennewens.one16.data;

public class WordCombinationDTO implements WordCombination {
    private String combination;
    private String resultWord;

    public WordCombinationDTO(String combination, String resultWord) {
        this.combination = combination;
        this.resultWord = resultWord;
    }

    public String getCombination() {
        return combination;
    }

    public String getResultWord() {
        return resultWord;
    }

    @Override
    public String toString() {
        return combination + " = " + resultWord;
    }

    @Override
    public int hashCode() {
        return combination.hashCode() + resultWord.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return hashCode() == obj.hashCode();
    }
}
