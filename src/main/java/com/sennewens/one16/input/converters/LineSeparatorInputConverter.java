package com.sennewens.one16.input.converters;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class LineSeparatorInputConverter {

    public static Set<String> convertInput(String input) {
        return new HashSet(Arrays.asList(input.split("\n")));
    }

}
