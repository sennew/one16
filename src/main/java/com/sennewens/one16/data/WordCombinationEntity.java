package com.sennewens.one16.data;

import javax.persistence.*;

@Entity
@Table(name = "WORDCOMBINATION")
public class WordCombinationEntity implements WordCombination {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    String combination;
    String resultword;

    public WordCombinationEntity() {
    }

    public WordCombinationEntity(WordCombination wordCombination) {
        setCombination(wordCombination.getCombination());
        setResultword(wordCombination.getResultWord());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCombination() {
        return combination;
    }

    public void setCombination(String combination) {
        this.combination = combination;
    }

    @Override
    public String getResultWord() {
        return resultword;
    }

    public void setResultword(String resultword) {
        this.resultword = resultword;
    }
}
